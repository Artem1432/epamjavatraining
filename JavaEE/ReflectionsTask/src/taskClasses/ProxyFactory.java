package taskClasses;

import annotations.Proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;

/**
 * Class for getting of instance of simple class
 * of proxy class when special annotation exist's.
 */
public class ProxyFactory {

    /**
     * @param proxyClass - class of future object.
     * @return class exemplar or proxy.
     * @throws Exception when something needed methods of using
     * classes don't exist.
     */
    public Object getInstanceOf(final Class proxyClass) throws Exception {
        Object result;
        if (proxyClass.isAnnotationPresent(Proxy.class)) {
            final Proxy proxy = (Proxy) proxyClass.getAnnotation(Proxy.class);
            final Class c = Class.forName(proxy.invocationHandler());
            Constructor constructor = c.getConstructor(Object.class);
            result = java.lang.reflect.Proxy.newProxyInstance(
                    proxyClass.getClassLoader(),
                    proxyClass.getInterfaces(),
                    (InvocationHandler) constructor.newInstance(proxyClass.newInstance())
            );
        } else {
            result = proxyClass.newInstance();
        }
        return result;
    }

}

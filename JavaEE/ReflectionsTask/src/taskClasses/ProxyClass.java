package taskClasses;

import annotations.Proxy;
import interfaces.IProxyInterface;

/**
 * Realisation of IProxyInterface.
 */
@Proxy(invocationHandler = "taskClasses.MyInvocationHandler")
public class ProxyClass implements IProxyInterface {

    @Override
    public String getHalloWorld() {
        return "Hallo world!";
    }

}

package taskClasses;

import annotations.Equal;
import enums.EqualKind;

/**
 * Class for testing of EqualClass.
 */
public class ClassForEqual {
    @Equal(equalKind = EqualKind.VALUE)
    private String s1;

    //@Equal(equalKind = EqualKind.VALUE)
    private String s2;

    public ClassForEqual(final String s1, final String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }
}

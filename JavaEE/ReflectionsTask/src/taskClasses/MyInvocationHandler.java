package taskClasses;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Implementation of InvocationHandler.
 */
public class MyInvocationHandler implements InvocationHandler {
    private Object object;

    /**
     * Create invocation handler with proxy object like field.
     * @param object - proxy object.
     */
    public MyInvocationHandler(final Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args)
            throws IllegalAccessException, InvocationTargetException {
        System.out.println("Method " + method.getName() + " was used");
        return method.invoke(object, args);
    }

}

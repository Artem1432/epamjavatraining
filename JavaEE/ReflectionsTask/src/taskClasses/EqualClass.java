package taskClasses;

import annotations.Equal;
import enums.EqualKind;

import java.lang.reflect.Field;

/**
 * Class with equal method.
 */
public class EqualClass {

    /**
     * Equal different objects by fields.
     * @param o1 - first object.
     * @param o2 - second object.
     * @return boolean result of equaling.
     * @throws IllegalAccessException when value of field can't be got
     * when they are private.
     * @throws NoSuchFieldException field of class does't exist.
     */
    public static boolean equalObjects(final Object o1, final Object o2)
            throws IllegalAccessException, NoSuchFieldException {
        boolean result;
        if (o1 != null && o2 != null && o1.getClass() == o2.getClass()) {
            result = equalByAnnotation(o1, o2);
        } else {
            result = o1 == o2;
        }
        return result;
    }

    private static boolean equalByAnnotation(final Object o1, final Object o2)
            throws IllegalAccessException, NoSuchFieldException {
        boolean result = true;
        final Field[] fields = o1.getClass().getDeclaredFields();
        for (Field field1 : fields) {
            if (!field1.isAnnotationPresent(Equal.class)) {
                continue;
            }
            EqualKind equalKind = field1.getAnnotation(Equal.class).equalKind();
            field1.setAccessible(true);
            Field field2 = o2.getClass().getDeclaredField(field1.getName());
            field2.setAccessible(true);
            if (equalKind == EqualKind.VALUE) {
                result = field1.get(o1).equals(field2.get(o2));
            } else if (equalKind == EqualKind.REFERENCE) {
                result = o1 == o2;
            }
            if (!result) {
                break;
            }
        }
        return result;
    }

}

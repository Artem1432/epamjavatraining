package enums;

/**
 * Enum with variants of equal.
 */
public enum EqualKind {
    /**
     * Variants of equal.
     */
    REFERENCE, VALUE
}

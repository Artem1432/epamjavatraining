package annotations;

import enums.EqualKind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation, which contains type of equal for EqualClass.
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface Equal {
    /**
     * @return equalKind.
     */
    EqualKind equalKind();
}

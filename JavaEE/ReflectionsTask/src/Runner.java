import interfaces.IProxyInterface;
import taskClasses.ClassForEqual;
import taskClasses.EqualClass;
import taskClasses.ProxyClass;
import taskClasses.ProxyFactory;

/**
 * Main class.
 */
public class Runner {

    /**
     * Main method.
     * @param args from command line.
     */
    public static void main(final String[] args) {
        try {
            ProxyFactory proxyFactory = new ProxyFactory();
            IProxyInterface proxyClass = (IProxyInterface) proxyFactory.getInstanceOf(ProxyClass.class);
            System.out.println(proxyClass.getHalloWorld());
            System.out.println(EqualClass.equalObjects(
                    new ClassForEqual("123", "456"), new ClassForEqual("123", "456")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

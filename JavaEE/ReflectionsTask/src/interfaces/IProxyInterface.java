package interfaces;

/**
 * This interface contains method for proxy.
 */
public interface IProxyInterface {

    /**
     * @return hallo world string in proxy class.
     */
    String getHalloWorld();

}

package validator;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.Target;

import java.io.File;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This class for validation of build.xml files.
 */
public class BuildValidator extends Task {

    /**
     * EMPTY_STRING is often using string.
     */
    private static final String EMPTY_STRING = "";
    /**
     * SPACE is often using string.
     */
    private static final String SPACE = " ";

    /**
     * Name of main target in build.xml file.
     */
    private static final String MAIN_TARGET_NAME = "main";

    /**
     * Part of massage which is printed when something target
     * has dependency.
     */
    private static final String SOMETHING_TARGET_HAS_DEPENDENCIES = "target has dependency.";

    /**
     * Massage which is printed when default target
     * is not set.
     */
    private static final String DEFAULT_TARGET_IS_NOT_SET = "Default target is not set.";

    /**
     * Pattern for target's names.
     */
    private static final Pattern TARGET_NAME_PATTERN = Pattern.compile("([A-Za-z]+[-]?)*[A-Za-z]");
    /**
     * Part of massage which is printed when something target
     * has bad name's format.
     */
    private static final String BAD_TARGET_NAME = "target has bad format of name.";
    /**
     * Massage which is printed when names of all targets
     * are tested.
     */
    private static final String RIGHT_BUILD_FILE = "build file is right.";

    /**
     * This field safe checkDepends task parameter.
     */
    private boolean checkDepends;

    /**
     * This field safe checkDefault task parameter.
     */
    private boolean checkDefault;

    /**
     * This field safe checkNames task parameter.
     */
    private boolean checkNames;

    /**
     * Reference to the parameter "project" from build-file-validator.xml.
     */
    private Project project;

    /**
     * This list contains testing build files.
     */
    private List<BuildFile> buildFileList = new LinkedList<>();

    public class BuildFile {
        /**
         * Reference on parameter "project" of a testing build file.
         */
        private Project project;
        /**
         * Path to a build file.
         */
        private String location;

        /**
         * It is empty constructor of BuildFile class.
         */
        public BuildFile() {
        }


        /**
         * This method create new instance of Project class if field is null or
         * return references to existing object.
         * @return references to existing object.
         */
        public Project getProject() {
            if (project == null) {
                project = new Project();
                ProjectHelper.configureProject(project, new File(location));
            }
            return project;
        }

        /**
         * This method for accept parameter "location" from build-file-validator.xml.
         * @param location - file location on disk.
         */
        public void setLocation(final String location) {
            this.location = location;
        }

        /**
         * @return location of build file.
         */
        public String getLocation() {
            return location;
        }
    }

    /**
     * This method for accept parameter "project" from build-file-validator.xml.
     * @param project .
     */
    @Override
    public void setProject(final Project project) {
        this.project = project;
    }

    /**
     * Method for setting of checkDepends task parameter
     * from build-file-validator.xml.
     * @param checkDepends .
     */
    public void setCheckDepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * Method for setting of checkDefault task parameter
     * from build-file-validator.xml.
     * @param checkDefault .
     */
    public void setCheckDefault(final boolean checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * Method for setting of checkNames task parameter
     * from build-file-validator.xml.
     * @param checkNames .
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Method for creating of a BuildFile created with
     * the ant format.
     * @return BuiltFile exemplar.
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFileList.add(buildFile);
        return buildFile;
    }

    /**
     * Main function of this task.
     * @throws BuildException .
     */
    @Override
    public void execute() throws BuildException {
        for (BuildFile buildFile : buildFileList) {
            Project testProject = buildFile.getProject();
            if (checkDefault) {
                checkDefault(testProject);
            }
            if (checkDepends) {
                checkDepends(testProject);
            }
            if (checkNames) {
                checkNames(testProject);
            }
            project.log(buildFile.getLocation() + SPACE + RIGHT_BUILD_FILE);
        }
    }

    /**
     * This method test dependencies of targets in a build file.
     * @param testProject is reference to parameter "project"
     *                    in testing a build file.
     */
    private void checkDepends(final Project testProject) {
        Enumeration<Target> targetEnumeration = testProject.getTargets().elements();
        while (targetEnumeration.hasMoreElements()) {
            Target target = targetEnumeration.nextElement();
            String targetName = target.getName();
            if (target.getDependencies().hasMoreElements() && !targetName.equals(MAIN_TARGET_NAME)) {
                throw new BuildException(targetName + SPACE + SOMETHING_TARGET_HAS_DEPENDENCIES);
            }
        }
    }

    /**
     * This method test names of targets in a build file.
     * @param testProject is reference to parameter "project"
     *                    in testing a build file.
     */
    private void checkNames(final Project testProject) {
        Enumeration<Target> targetEnumeration = testProject.getTargets().elements();
        while (targetEnumeration.hasMoreElements()) {
            String targetName = targetEnumeration.nextElement().getName();
            if (!TARGET_NAME_PATTERN.matcher(targetName).matches()
                    && !EMPTY_STRING.equals(targetName)) {
                throw new BuildException(targetName + SPACE + BAD_TARGET_NAME);
            }
        }
    }

    /**
     * This method test existing of a default target in build file.
     * @param testProject is reference to parameter "project"
     *                    in testing a build file.
     */
    private void checkDefault(final Project testProject) {
        if (testProject.getDefaultTarget() == null) {
            throw new BuildException(DEFAULT_TARGET_IS_NOT_SET);
        }
    }
}

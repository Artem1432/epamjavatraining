import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class test all program.
 */
public class RunnerTest {

    /**
     * This test activate elevator algorithm 100 times.
     */
    @Test
    public void main() {
        for (int i = 0; i < 100; i++) {
            Runner.main(null);
        }
        System.out.println("Success!");
    }
}
package elevator.generator;

import elevator.beans.Passenger;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Generate passengers with unique id.
 */
public class PassengerGenerator {

    private final int floorCount;
    private static AtomicInteger passengerCounter = new AtomicInteger(0);

    /**
     * Generator's constructor.
     * @param floorCount - count of floors for generation.
     */
    public PassengerGenerator(final int floorCount) {
        this.floorCount = floorCount;
    }

    /**
     * @return generated passenger.
     */
    public Passenger generate() {
        final Random random = new Random();
        int startFloor = random.nextInt(floorCount);
        int destFloor = 0;
        do {
            destFloor = random.nextInt(floorCount);
        } while (destFloor == startFloor);
        return new Passenger(passengerCounter.incrementAndGet(), startFloor, destFloor);
    }

}

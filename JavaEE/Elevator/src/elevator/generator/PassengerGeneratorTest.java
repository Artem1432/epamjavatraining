package elevator.generator;

import elevator.beans.Passenger;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test of PassengerGenerator.
 */
public class PassengerGeneratorTest {

    /**
     * Test generate method.
     */
    @Test
    public void generate() {
        final PassengerGenerator passengerGenerator =
                new PassengerGenerator(9);
        final Passenger p1 = passengerGenerator.generate();
        final Passenger p2 = passengerGenerator.generate();
        assertEquals(p1.equals(p2), false);
    }
}
package elevator.interfaces;

/**
 * Interface of container.
 * @param <T> - type of container's contents.
 */
public interface Container<T> {

    /**
     * Add passenger.
     * @param passenger - adding passenger.
     */
    void addPassenger(final T passenger);

    /**
     * Remove passenger.
     * @param passenger - removing passenger.
     */
    void removePassenger(final T passenger);

}

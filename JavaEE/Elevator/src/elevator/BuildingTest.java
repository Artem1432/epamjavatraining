package elevator;

import elevator.beans.Passenger;
import elevator.enums.TransportationState;
import elevator.implementations.Floor;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test class of building.
 * Method activateElevator() is not here because
 * it is tested as part of RunnerTest.
 */
public class BuildingTest extends Assert {

    /**
     * Test of putPassenger().
     */
    @Test
    public void putPassenger() {
        final Building building = new Building(5, 9);
        final Passenger passenger = new Passenger(1, 1, 2);
        building.putPassenger(passenger);
        Floor floor = building.getFloor(1);
        assertEquals(floor.getOutUpContainer().contains(passenger), true);
        assertEquals(passenger.getContainer(), floor);
    }

    /**
     * Test of isAllPassengers().
     */
    @Test
    public void isAllPassengers() {
        final Building building = new Building(5, 9);
        assertEquals(building.isAllPassengers(0), true);
        final Passenger passenger = new Passenger(1, 1, 2);
        building.putPassenger(passenger);
        assertNotEquals(building.isAllPassengers(1), true);
        building.getFloor(1)
                .getOutUpContainer().remove(passenger);
        building.getFloor(2)
                .getInContainer().add(passenger);
        passenger.setTransportationState(TransportationState.COMPLETED);
        assertEquals(building.isAllPassengers(1), true);
    }

    /**
     * Teest of getFloor().
     */
    @Test
    public void getFloor() {
        final Building building = new Building(5, 9);
        Floor floor = building.getFloor(4);
        assertEquals(floor.getFloorNumber(), 4);
    }

    /**
     * Test of isEmptyFloors().
     */
    @Test
    public void isEmptyFloors() {
        final Building building = new Building(5, 9);
        assertEquals(building.isEmptyFloors(), true);
        final Passenger passenger = new Passenger(1, 1, 2);
        building.putPassenger(passenger);
        assertEquals(building.isEmptyFloors(), false);
        building.getFloor(1)
                .getOutUpContainer().remove(passenger);
        building.getFloor(2)
                .getInContainer().add(passenger);
        assertEquals(building.isEmptyFloors(), true);
    }
}
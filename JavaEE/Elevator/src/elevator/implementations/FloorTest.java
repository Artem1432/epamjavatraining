package elevator.implementations;

import elevator.beans.Passenger;
import elevator.enums.Direction;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test of floor.
 */
public class FloorTest {

    @Test
    public void exitOnFloor() {
        final Floor floor = new Floor(1);
        final Passenger passenger = new Passenger(1, 0, 1);
        assertEquals(floor.getInContainer().contains(passenger), false);
        floor.exitOnFloor(passenger);
        assertEquals(floor.getInContainer().contains(passenger), true);
    }

    @Test
    public void addPassenger() {
        final Floor floor = new Floor(1);
        final Passenger p1 = new Passenger(1, 0, 1);
        floor.addPassenger(p1);
        assertEquals(floor.getOutUpContainer().contains(p1), true);
        final Passenger p2 = new Passenger(1, 1, 0);
        floor.addPassenger(p2);
        assertEquals(floor.getOutDownContainer().contains(p1), true);
    }

    @Test
    public void removePassenger() {
        final Floor floor = new Floor(1);
        final Passenger p1 = new Passenger(1, 0, 1);
        floor.addPassenger(p1);
        assertEquals(floor.getOutUpContainer().contains(p1), true);
        floor.removePassenger(p1);
        assertEquals(floor.getOutUpContainer().contains(p1), false);
        final Passenger p2 = new Passenger(1, 1, 0);
        floor.addPassenger(p2);
        assertEquals(floor.getOutDownContainer().contains(p1), true);
        floor.removePassenger(p2);
        assertEquals(floor.getOutDownContainer().contains(p1), false);

    }

    @Test
    public void isEmpty() {
        final Floor floor = new Floor(1);
        assertEquals(floor.isEmpty(Direction.STOP), true);
        assertEquals(floor.isEmpty(Direction.UP), true);
        assertEquals(floor.isEmpty(Direction.DOWN), true);
        final Passenger p1 = new Passenger(1, 0, 1);
        floor.addPassenger(p1);
        assertEquals(floor.isEmpty(Direction.STOP), false);
        assertEquals(floor.isEmpty(Direction.UP), false);
        assertEquals(floor.isEmpty(Direction.DOWN), true);
    }

    @Test
    public void getContainer() {
        final Floor floor = new Floor(1);
        assertEquals(floor.getContainer(Direction.UP), floor.getOutUpContainer());
        assertEquals(floor.getContainer(Direction.DOWN), floor.getOutDownContainer());
    }
}
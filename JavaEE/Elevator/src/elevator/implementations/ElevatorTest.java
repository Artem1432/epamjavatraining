package elevator.implementations;

import elevator.beans.Passenger;
import elevator.enums.Direction;
import elevator.generator.PassengerGenerator;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test of elevator.
 */
public class ElevatorTest {

    @Test
    public void isExiting() {
        final Elevator elevator = new Elevator(5, 9);
        elevator.addPassenger(new Passenger(1, 1, 2));
        elevator.setElevatorPosition(2);
        assertEquals(elevator.isExiting(), true);
        elevator.setElevatorPosition(1);
        assertEquals(elevator.isExiting(), false);
    }

    @Test
    public void exit() {
        final Elevator elevator = new Elevator(5, 9);
        final Floor floor = new Floor(1);
        final Passenger passenger = new Passenger(1, 0, 1);
        elevator.addPassenger(passenger);
        assertEquals(elevator.getElevatorContainer().contains(passenger), true);
        assertEquals(floor.getInContainer().contains(passenger), false);
        passenger.setExitFlag(true);
        elevator.exit(floor);
        assertEquals(elevator.getElevatorContainer().contains(passenger), false);
        assertEquals(floor.getInContainer().contains(passenger), true);
    }

    @Test
    public void enter() {
        final Elevator elevator = new Elevator(5, 9);
        final Floor floor = new Floor(1);
        final Passenger passenger = new Passenger(1, 0, 1);
        floor.addPassenger(passenger);
        assertEquals(floor.getOutUpContainer().contains(passenger), true);
        assertEquals(elevator.getElevatorContainer().contains(passenger), false);
        elevator.enter(floor);
        assertEquals(floor.getOutUpContainer().contains(passenger), false);
        assertEquals(elevator.getElevatorContainer().contains(passenger), true);
    }

    @Test
    public void moveElevator() {
        final Elevator elevator = new Elevator(5, 9);
        assertEquals(elevator.getElevatorPosition(), 0);
        elevator.moveElevator();
        assertEquals(elevator.getElevatorPosition(), 1);
        elevator.setElevatorPosition(8);
        assertEquals(elevator.getDirection(), Direction.UP);
        elevator.moveElevator();
        assertEquals(elevator.getDirection(), Direction.DOWN);
        elevator.setElevatorPosition(0);
        elevator.moveElevator();
        assertEquals(elevator.getDirection(), Direction.UP);
    }

    @Test
    public void isEmpty() {
        final Elevator elevator = new Elevator(5, 9);
        assertEquals(elevator.isEmpty(), true);
        elevator.addPassenger(new Passenger(1, 0, 1));
        assertEquals(elevator.isEmpty(), false);
    }

    @Test
    public void isFull() {
        final Elevator elevator = new Elevator(5, 9);
        final PassengerGenerator generator = new PassengerGenerator(9);
        for (int i = 0; i < 5; i++) {
            elevator.addPassenger(generator.generate());
        }
        assertEquals(elevator.isFull(), true);
    }
}
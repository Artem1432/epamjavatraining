package elevator.implementations;

import elevator.beans.Passenger;
import elevator.constants.LogConstants;
import elevator.enums.Direction;
import elevator.interfaces.Container;
import elevator.reporter.Reporter;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Class of elevator.
 */
public class Elevator implements Container<Passenger> {

    private final ConcurrentLinkedQueue<Passenger> elevatorContainer =
            new ConcurrentLinkedQueue<>();
    private int elevatorPosition = 0;
    private int passengersCount = 0;
    private final int elevatorCapacity;
    private final int floorCount;
    private Direction direction = Direction.UP;

    private static final Logger LOGGER = Logger.getLogger(Elevator.class);

    /**
     * Elevator constructor.
     * @param elevatorCapacity - elevator's size.
     * @param floorCount - count of building's floors.
     */
    public Elevator(final int elevatorCapacity, final int floorCount) {
        this.elevatorCapacity = elevatorCapacity;
        this.floorCount = floorCount;
    }

    @Override
    public void addPassenger(final Passenger passenger) {
        if (passengersCount < elevatorCapacity) {
            elevatorContainer.add(passenger);
            passengersCount++;
        }
    }

    @Override
    public void removePassenger(final Passenger passenger) {
        if (passengersCount > 0) {
            elevatorContainer.remove(passenger);
            passengersCount--;
        }
    }

    /**
     * Try to find passenger which exit on this floor.
     * @return search result.
     */
    public boolean isExiting() {
        for (Passenger passenger : elevatorContainer) {
            if (passenger.getDestinationFloorNumber() == elevatorPosition) {
                return true;
            }
        }
        return false;
    }

    /**
     * Transport passengers on floor.
     * @param floor - dest floor.
     */
    public void exit(final Floor floor) {
        Iterator<Passenger> iterator = elevatorContainer.iterator();
        while (iterator.hasNext()) {
            Passenger passenger = iterator.next();
            if (passenger.getExitFlag()) {
                floor.exitOnFloor(passenger);
                iterator.remove();
                passengersCount--;
                passenger.setContainer(floor);
                Reporter.logAndReport(LOGGER,
                        String.format(LogConstants.DEBOARDING_OF_PASSENGER,
                                passenger.getId(), floor.getFloorNumber()));
            }
        }
    }

    /**
     * Transport passengers on elevator.
     * @param floor - start floor.
     */
    public void enter(final Floor floor) {
        ConcurrentLinkedDeque<Passenger> container = floor.getContainer(direction);
        while (passengersCount < elevatorCapacity && !container.isEmpty()) {
            synchronized (this) {
                notifyAll();
            }
            Passenger passenger = container.getFirst();
            if (passenger.getEnterFlag()) {
                addPassenger(passenger);
                container.removeFirst();
                passenger.setContainer(this);
                Reporter.logAndReport(LOGGER,
                        String.format(LogConstants.BOARDING_OF_PASSENGER,
                                passenger.getId(), floor.getFloorNumber()));
            }
        }
    }

    /**
     * Move or change direction of floor.
     */
    public void moveElevator() {
        switch (direction) {
            case UP:
                if (elevatorPosition < floorCount - 1) {
                    Reporter.logAndReport(LOGGER,
                            String.format(LogConstants.MOVING_ELEVATOR,
                                    elevatorPosition, ++elevatorPosition));
                } else {
                    direction = Direction.DOWN;
                }
                break;
            case DOWN:
                if (elevatorPosition > 0) {
                    Reporter.logAndReport(LOGGER,
                            String.format(LogConstants.MOVING_ELEVATOR,
                                    elevatorPosition, --elevatorPosition));
                } else {
                    direction = Direction.UP;
                }
                break;
            default:
                break;
        }
    }

    /**
     * @return result of testing on emptiness of container.
     */
    public boolean isEmpty() {
        return elevatorContainer.isEmpty();
    }

    /**
     * @return result of equaling count of passengers in elevator
     * with elevator capacity.
     */
    public boolean isFull() {
        return passengersCount == elevatorCapacity;
    }

    /**
     * Set passenger's count in elevator.
     * @param passengersCount - future passenger's count.
     */
    public void setPassengersCount(final int passengersCount) {
        this.passengersCount = passengersCount;
    }

    /**
     * Ser elevator position.
     * @param elevatorPosition - future elevator position.
     */
    public void setElevatorPosition(final int elevatorPosition) {
        this.elevatorPosition = elevatorPosition;
    }

    /**
     * Set elevator direction.
     * @param direction - future elevator direction.
     */
    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    /**
     * @return passenger's count.
     */
    public int getPassengersCount() {
        return passengersCount;
    }

    /**
     * @return elevator position.
     */
    public int getElevatorPosition() {
        return elevatorPosition;
    }

    /**
     * @return elevator direction.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @return floor's count.
     */
    public int getFloorCount() {
        return floorCount;
    }

    /**
     * @return capacity of elevator.
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * @return elevator container.
     */
    public ConcurrentLinkedQueue<Passenger> getElevatorContainer() {
        return elevatorContainer;
    }
}

package elevator.implementations;

import elevator.beans.Passenger;
import elevator.enums.Direction;
import elevator.interfaces.Container;

import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Class of floor.
 */
public class Floor implements Container<Passenger> {

    private final int floorNumber;
    private final ConcurrentLinkedQueue<Passenger> inContainer =
            new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedDeque<Passenger> outUpContainer =
            new ConcurrentLinkedDeque<>();
    private final ConcurrentLinkedDeque<Passenger> outDownContainer =
            new ConcurrentLinkedDeque<>();

    /**
     * Constructor of class.
     * @param floorNumber - floor's number.
     */
    public Floor(final int floorNumber) {
        this.floorNumber = floorNumber;
    }

    /**
     * Add passenger in dest container.
     * @param passenger - coming passenger.
     */
    public void exitOnFloor(final Passenger passenger) {
        inContainer.add(passenger);
    }

    @Override
    public void addPassenger(final Passenger passenger) {
        switch (passenger.getDirection()) {
            case UP:
                outUpContainer.addLast(passenger);
                break;
            case DOWN:
                outDownContainer.addLast(passenger);
                break;
            default:
                break;
        }
    }

    @Override
    public void removePassenger(final Passenger passenger) {
        switch (passenger.getDirection()) {
            case UP:
                outUpContainer.removeFirst();
                break;
            case DOWN:
                outDownContainer.removeFirst();
                break;
            default:
                break;
        }
    }

    /**
     * @param direction - direction for container.
     * @return result of empty test of container by direction.
     */
    public boolean isEmpty(final Direction direction) {
        switch (direction) {
            case UP:
                return outUpContainer.isEmpty();
            case DOWN:
                return outDownContainer.isEmpty();
            default:
                return outUpContainer.isEmpty() && outDownContainer.isEmpty();
        }
    }

    /**
     * @param direction - direction for container.
     * @return container by direction.
     */
    public ConcurrentLinkedDeque<Passenger> getContainer(final Direction direction) {
        switch (direction) {
            case UP:
                return outUpContainer;
            case DOWN:
                return outDownContainer;
            default:
                return null;
        }
    }

    /**
     * @return floor number.
     */
    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * @return dest container.
     */
    public ConcurrentLinkedQueue<Passenger> getInContainer() {
        return inContainer;
    }

    /**
     * @return OutDownContainer.
     */
    public ConcurrentLinkedDeque<Passenger> getOutDownContainer() {
        return outDownContainer;
    }

    /**
     * @return OutUpContainer.
     */
    public ConcurrentLinkedDeque<Passenger> getOutUpContainer() {
        return outUpContainer;
    }

    /**
     * @param obj - equaling object.
     * @return result of equaling.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return ((Floor) obj).floorNumber == floorNumber;
    }

    @Override
    public int hashCode() {
        return floorNumber;
    }
}

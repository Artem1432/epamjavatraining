package elevator.enums;

/**
 * Enum for state of passenger's transportation.
 */
public enum TransportationState {

    NOT_STARTED, IN_PROGRESS, COMPLETED

}

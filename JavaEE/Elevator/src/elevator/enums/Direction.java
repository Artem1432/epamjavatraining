package elevator.enums;

/**
 * Enum of directions of passengers or elevator.
 */
public enum Direction {

    UP, DOWN, STOP

}

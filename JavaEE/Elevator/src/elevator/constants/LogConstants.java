package elevator.constants;

public class LogConstants {

    public final static String STARTING_TRANSPORTATION =
            "STARTING_TRANSPORTATION";
    public final static String COMPLETION_TRANSPORTATION =
            "COMPLETION_TRANSPORTATION";
    public final static String MOVING_ELEVATOR =
            "MOVING_ELEVATOR (from story-%d to story-%d)";
    public final static String BOARDING_OF_PASSENGER =
            "BOARDING_OF_PASSENGER (passenger %d on story-%d)";
    public final static String DEBOARDING_OF_PASSENGER =
            "DEBOARDING_OF_PASSENGER (passenger %d on story-%d)";
}

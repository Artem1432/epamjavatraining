package elevator.task;

import elevator.Building;
import elevator.enums.TransportationState;
import elevator.generator.PassengerGenerator;
import elevator.beans.Passenger;
import elevator.implementations.Elevator;
import elevator.reporter.Reporter;
import org.apache.log4j.Logger;

/**
 * Transportation task.
 */
public class TransportationTask extends Reporter implements Runnable {

    private final Building building;

    public TransportationTask(final Building building) {
        this.building = building;
    }

    @Override
    public void run() {
        final Logger logger = Logger.getLogger(TransportationTask.class);
        final Elevator elevator = building.getElevator();
        final PassengerGenerator generator = new PassengerGenerator(elevator.getFloorCount());
        final Passenger passenger = generator.generate();
        building.putPassenger(passenger);
        try {
            passenger.setTransportationState(TransportationState.IN_PROGRESS);
            logAndReport(logger, "Passenger " + passenger.getId()
                    + " is situated on the start floor " + passenger.getStartFloorNumber() + ".");
            while (passenger.getContainer() != elevator) {
                synchronized (elevator) {
                    elevator.wait();
                }
                if (elevator.getDirection() == passenger.getDirection()
                        && elevator.getElevatorPosition() == passenger.getStartFloorNumber()) {
                    passenger.setEnterFlag(true);
                    synchronized (elevator) {
                        elevator.wait();
                    }
                } else {
                    passenger.setEnterFlag(false);
                }
            }
            System.out.println("Passenger " + passenger.getId() + " test");
            logAndReport(logger, "Passenger " + passenger.getId() + " is situated in the elevator.");
            synchronized (elevator) {
                while (elevator.getElevatorPosition() != passenger.getDestinationFloorNumber()) {
                    elevator.wait();
                }
            }
            passenger.setExitFlag(true);
            synchronized (elevator) {
                while (passenger.getContainer() != building.getFloor(passenger.getDestinationFloorNumber())) {
                    elevator.wait();
                }
            }
            passenger.setTransportationState(TransportationState.COMPLETED);
            logAndReport(logger, "Passenger " + passenger.getId()
                    + " is come on the dest floor " + passenger.getDestinationFloorNumber() + ".");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package elevator.beans;

import elevator.enums.Direction;
import org.junit.Test;

import static org.junit.Assert.*;

public class PassengerTest {

    @Test
    public void getDirection() {
        final Passenger passenger1 = new Passenger(1, 1, 2);
        assertEquals(passenger1.getDirection(), Direction.UP);
        final Passenger passenger2 = new Passenger(2, 3, 2);
        assertEquals(passenger2.getDirection(), Direction.DOWN);
        final Passenger passenger3 = new Passenger(3, 2, 2);
        assertEquals(passenger3.getDirection(), Direction.STOP);
    }
}
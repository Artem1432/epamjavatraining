package elevator.beans;

import elevator.enums.Direction;
import elevator.enums.TransportationState;
import elevator.interfaces.Container;

/**
 * Class of passenger.
 */
public class Passenger {

    private final int id;
    private final int startFloorNumber;
    private final int destinationFloorNumber;
    private boolean exitFlag = false;
    private boolean enterFlag = false;
    private TransportationState transportationState =
            TransportationState.NOT_STARTED;
    private Container<Passenger> container = null;

    /**
     * Constructor of class
     * @param id - passenger's id.
     * @param startFloorNumber - start floor number of passenger.
     * @param destinationFloorNumber - dest floor number of passenger.
     */
    public Passenger(final int id, final int startFloorNumber,
                     final int destinationFloorNumber) {
        this.id = id;
        this.startFloorNumber = startFloorNumber;
        this.destinationFloorNumber = destinationFloorNumber;
    }

    /**
     * @return direction of passenger movement by his start and dest
     * floors.
     */
    public Direction getDirection() {
        Direction result = Direction.STOP;
        if (destinationFloorNumber > startFloorNumber) {
           result = Direction.UP;
        } else if (destinationFloorNumber < startFloorNumber) {
            result = Direction.DOWN;
        }
        return result;
    }

    /**
     * Equal passenger with other object.
     * @param obj - equaling object.
     * @return result of equal.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Passenger passenger = (Passenger) obj;
        return passenger.id == id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Set exit flag.
     * @param exitFlag - future value of exit flag.
     */
    public synchronized void setExitFlag(final boolean exitFlag) {
        this.exitFlag = exitFlag;
    }

    /**
     * Set enter flag.
     * @param enterFlag - future value of enter flag.
     */
    public synchronized void setEnterFlag(final boolean enterFlag) {
        this.enterFlag = enterFlag;
    }

    /**
     * Set container where passenger must be now.
     * @param container - reference on container.
     */
    public void setContainer(final Container<Passenger> container) {
        this.container = container;
    }

    /**
     * Set transportation state.
     * @param transportationState - future meaning of transportation state.
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    /**
     * @return id.
     */
    public int getId() {
        return id;
    }

    /**
     * @return number of start floor.
     */
    public int getStartFloorNumber() {
        return startFloorNumber;
    }

    /**
     * @return number of dest floor.
     */
    public int getDestinationFloorNumber() {
        return destinationFloorNumber;
    }

    /**
     * @return exit flag.
     */
    public synchronized boolean getExitFlag() {
        return exitFlag;
    }

    /**
     * @return enter flag.
     */
    public synchronized boolean getEnterFlag() {
        return enterFlag;
    }

    /**
     * @return container.
     */
    public Container<Passenger> getContainer() {
        return container;
    }

    /**
     * @return transportation state.
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }
}

package elevator;

import elevator.beans.Passenger;
import elevator.enums.Direction;
import elevator.enums.TransportationState;
import elevator.implementations.Elevator;
import elevator.implementations.Floor;
import elevator.reporter.Reporter;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class collect algorithm of elevator.
 */
public class Building extends Reporter {

    private final List<Floor> floorList = new ArrayList<>();
    private final int floorCount;
    private final Elevator elevator;
    private final Lock lock = new ReentrantLock(true);
    private static final Logger LOGGER = Logger.getLogger(Building.class);

    /**
     * Building constructor.
     * @param elevatorCapacity - capacity of elevator.
     * @param floorCount - count of floors.
     */
    public Building(final int elevatorCapacity, final int floorCount) {
        this.elevator = new Elevator(elevatorCapacity, floorCount);
        this.floorCount = floorCount;
        createFloors(floorCount);
    }

    /**
     * Main method of this class. It is elevator algorithm.
     * @throws InterruptedException because of problems with threads.
     */
    public void activateElevator() throws InterruptedException {
        Condition condition = lock.newCondition();
        while (true) {
            int i = 0;
            while (elevator.isEmpty() && isEmptyFloors()) {
                i++;
                logAndReport(LOGGER, "Elevator wait passengers.");
                lock.lock();
                try {
                    condition.await(1, TimeUnit.SECONDS);
                } finally {
                    lock.unlock();
                }
                if (i == 4) {
                    return;
                }
            }
            lock.lock();
            try {
                int elevatorPosition = elevator.getElevatorPosition();
                Floor thisFloor = floorList.get(elevatorPosition);
                if (elevator.isExiting() || (!thisFloor.isEmpty(elevator.getDirection())
                        && !elevator.isFull())) {
                    synchronized (elevator) {
                        elevator.notifyAll();
                    }
                    //condition.await(1, TimeUnit.SECONDS);
                    elevator.exit(thisFloor);
                    elevator.enter(thisFloor);
                    synchronized (elevator) {
                        elevator.notifyAll();
                    }
                } else {
                    elevator.moveElevator();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Test all floor dispatch containers on existence of passengers.
     * @return test result.
     */
    public boolean isEmptyFloors() {
        lock.lock();
        try {
            for (Floor floor : floorList) {
                if (!floor.isEmpty(Direction.STOP)) {
                    return false;
                }
            }
            return true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Add passenger in building.
     * @param passenger - adding passenger.
     */
    public void putPassenger(final Passenger passenger) {
        Floor floor = floorList.get(passenger.getStartFloorNumber());
        floor.addPassenger(passenger);
        passenger.setContainer(floor);
    }

    /**
     * Final test for passengers.
     * @param passengersCount - passenger's count.
     * @return test result.
     */
    public boolean isAllPassengers(final int passengersCount) {
        boolean result1 = true, result2 = true, result3 = true;
        result1 = elevator.isEmpty() && isEmptyFloors();
        if (!result1) {
            return result1;
        }
        result2 = passengersFinalTest();
        int count = 0;
        for (Floor floor : floorList) {
            count += floor.getInContainer().size();
        }
        if (count != passengersCount) {
            result3 = false;
        }
        return result2 && result3;
    }

    private boolean passengersFinalTest() {
        for (Floor floor : floorList) {
            for (Passenger p : floor.getInContainer()) {
                if (p.getTransportationState()
                        != TransportationState.COMPLETED
                        || floor.getFloorNumber() != p.getDestinationFloorNumber()) {
                    return false;
                }
            }
        }
        return true;
    }

    private void createFloors(final int floorCount) {
        for (int i = 0; i < floorCount; i++) {
            floorList.add(new Floor(i));
        }
    }

    /**
     * @return list of floors.
     */
    public List<Floor> getFloorList() {
        return floorList;
    }

    /**
     * @return elevator.
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * @param floorNumber - number of floor.
     * @return floor of building by floor's number.
     */
    public Floor getFloor(final int floorNumber) {
        return floorList.get(floorNumber);
    }
}

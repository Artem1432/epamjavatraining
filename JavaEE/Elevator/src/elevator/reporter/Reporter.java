package elevator.reporter;

import org.apache.log4j.Logger;

public abstract class Reporter {

    public static void logAndReport(final Logger logger, final String message) {
        logger.info(message);
        System.out.println(message);
    }

}

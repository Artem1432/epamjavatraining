import elevator.Building;
import elevator.constants.LogConstants;
import elevator.implementations.Elevator;
import elevator.reporter.Reporter;
import elevator.task.TransportationTask;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.*;

import org.apache.log4j.*;

/**
 * It is Runner/
 */
public class Runner extends Reporter {

    private static final Logger LOGGER = Logger.getLogger(Runner.class);;

    /**
     * Main method of application.
     * @param args is array of parameters from command line.
     */
    public static void main(final String[] args) {
        try {
            int elevatorCapacity, floorCount, passengerCount;
            final ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            floorCount = Integer.parseInt(resourceBundle.getString("floorCount"));
            elevatorCapacity = Integer.parseInt(resourceBundle.getString("elevatorCapacity"));
            passengerCount = Integer.parseInt(resourceBundle.getString("passengerCount"));
            logAndReport(LOGGER, "Config data is read.");
            final Building building = new Building(elevatorCapacity, floorCount);
            logAndReport(LOGGER, "Building is created.");
            final Elevator elevator = building.getElevator();
            final TransportationTask task = new TransportationTask(building);
            final ExecutorService executorService = Executors.newFixedThreadPool(passengerCount);
            final List<Future> futureList = new ArrayList<>();
            for (int i = 0; i < passengerCount; i++) {
                futureList.add(executorService.submit(task));
            }
            logAndReport(LOGGER, LogConstants.STARTING_TRANSPORTATION);
            building.activateElevator();
            try {
                for (int i = 0; i < passengerCount; i++) {
                    futureList.get(i).get();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            executorService.shutdown();
            if (building.isAllPassengers(passengerCount)) {
                logAndReport(LOGGER, "All passengers arrived.");
            } else {
                logAndReport(LOGGER, "Somebody was lost.");
            }
            logAndReport(LOGGER, LogConstants.COMPLETION_TRANSPORTATION);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

package implementation;

import interfaces.Cache;

import java.util.*;

/**
 * LFU cache implementation.
 * @param <K> - type of keys.
 * @param <V> - type of values.
 */
public class LFUCache<K, V> implements Cache<K, V> {

    private class Key<T> {
        private T key;
        private int dequeNumber;

        Key(final T key) {
            this.key = key;
        }

        /**
         * Key constructor.
         * @param key of value.
         * @param dequeNumber number of deque with key.
         */
        Key(final T key, final int dequeNumber) {
            this.key = key;
            this.dequeNumber = dequeNumber;
        }

        /**
         * @return key.
         */
        public T getKey() {
            return key;
        }

        /**
         * @return deque's number.
         */
        public int getDequeNumber() {
            return dequeNumber;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj == this) {
                return true;
            }
            boolean result = false;
            if (obj instanceof LFUCache.Key) {
                result = key.equals(((Key<T>) obj).key);
            }
            return result;
        }

        @Override
        public int hashCode() {
            return key.hashCode();
        }
    }

    private final Map<Key<K>, V> cacheMap = new LinkedHashMap<>();
    private final List<Deque<K>> dequeKeyList = new LinkedList<>();
    private final int cacheSize;
    private final double outCount;

    /**
     * LFU cache constructor.
     * @param cacheSize - size of cache.
     * @param outFactor - out factor.
     */
    public LFUCache(final int cacheSize, final double outFactor) {
        this.cacheSize = cacheSize;
        this.outCount = outFactor * cacheSize;
        for (int i = 0; i < cacheSize; i++) {
            dequeKeyList.add(new ArrayDeque<>());
        }
    }

    @Override
    public synchronized void putValue(final K key, final V value) {
        int dequeIndex = 0;
        if (!cacheMap.containsKey(new Key<K>(key))) {
            cleanCache();
        } else {
            Key<K> k = findKey(key);
            dequeKeyList.get(k.dequeNumber).remove(k.key);
            if (k.dequeNumber < cacheSize - 1) {
                k.dequeNumber++;
            }
            dequeIndex = k.dequeNumber;
        }
        dequeKeyList.get(dequeIndex).addLast(key);
        cacheMap.put(new Key<>(key, dequeIndex), value);
    }

    @Override
    public synchronized void cleanCache() {
        if (cacheMap.size() == cacheSize) {
            deleteElements((int) (outCount));
        }
    }

    @Override
    public V getValue(final K key) {
        synchronized (this) {
            V result = null;
            Key<K> k = findKey(key);
            if (k != null) {
                dequeKeyList.get(k.dequeNumber).remove(k.key);
                if (k.dequeNumber < cacheSize - 1) {
                    k.dequeNumber++;
                }
                dequeKeyList.get(k.dequeNumber).addLast(key);
                result = cacheMap.get(new Key<>(key));
            }
            return result;
        }
    }

    @Override
    public Collection<K> getKeys() {
        synchronized (cacheMap) {
            final List<K> keyList = new LinkedList<>();
            cacheMap.keySet().stream()
                    .forEach(k -> keyList.add(k.key));
            return keyList;
        }
    }

    @Override
    public Collection<V> getValues() {
        return cacheMap.values();
    }

    private void deleteElements(final int outElementCount) {
        findDeletingKeys(outElementCount).stream()
                .forEach(key -> {
                    cacheMap.remove(key);
                    dequeKeyList.get(key.dequeNumber).remove(key.key);
                });
    }

    private List<Key> findDeletingKeys(final int outElementCount) {
        final List<Key> keyList = new LinkedList<>();
        int counter = 0;
        while (true) {
            for (int i = 0; i < dequeKeyList.size(); i++) {
                for (K key : dequeKeyList.get(i)) {
                    if (counter == outElementCount) {
                        return keyList;
                    }
                    keyList.add(new Key<>(key, i));
                    counter++;
                }
            }
        }
    }

    private Key<K> findKey(final K key) {
        synchronized (cacheMap) {
            Key<K> result = null, newKey = new Key<>(key);
            for (Key<K> k : cacheMap.keySet()) {
                if (k.equals(newKey)) {
                    result = k;
                    break;
                }
            }
            return result;
        }
    }

    /**
     * @return cache size.
     */
    public int getCacheSize() {
        return cacheSize;
    }
}

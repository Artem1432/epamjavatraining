package implementation;

import interfaces.Cache;

import java.util.*;

/**
 * LRU cache implementation.
 * @param <K> - type of key.
 * @param <V> - type of value.
 */
public class LRUCache<K, V> implements Cache<K, V> {

    private final Map<K, V> cacheMap = new LinkedHashMap<>();
    private final Map<K, Long> timeMap = new LinkedHashMap<>();
    private final int cacheSize;

    /**
     * LRUCache constructor.
     * @param cacheSize - size of cache.
     */
    public LRUCache(final int cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    public synchronized void putValue(final K key, final V value) {
        if (!cacheMap.containsKey(key)) {
            cleanCache();
        }
        cacheMap.put(key, value);
        timeMap.put(key, System.nanoTime());
    }



    @Override
    public void cleanCache() {
        synchronized (this) {
            if (cacheMap.size() == cacheSize) {
                K minKey = findMinKey();
                cacheMap.remove(minKey);
                timeMap.remove(minKey);
            }
        }
    }

    private K findMinKey() {
        K minKey = null;
        Optional<Map.Entry<K, Long>> optional = timeMap.entrySet().stream()
                .min(Comparator.comparingLong(Map.Entry::getValue));
        if (optional.isPresent()) {
            minKey = optional.get().getKey();
        }
        return minKey;
    }

    /**
     * @return size of cache.
     */
    public int getCacheSize() {
        return cacheSize;
    }

    @Override
    public V getValue(final K key) {
        V result = null;
        synchronized (this) {
            if (timeMap.containsKey(key)) {
                timeMap.put(key, System.nanoTime());
                result = cacheMap.get(key);
            }
            return result;
        }
    }

    @Override
    public Collection<V> getValues() {
        return cacheMap.values();
    }

    @Override
    public Collection<K> getKeys() {
        return cacheMap.keySet();
    }
}

package interfaces;

import java.util.Collection;

/**
 * Cache template.
 * @param <K> - type of key.
 * @param <V> - type of value.
 */
public interface Cache<K, V> {

    /**
     * Add data in cache.
     * @param key - key for value.
     * @param value - adding data.
     */
    void putValue(K key, V value);

    /**
     * Clean a part of cache
     * when it is full.
     */
    void cleanCache();

    /**
     * @param key - key for data.
     * @return data by key.
     */
    V getValue(K key);

    /**
     * @return collection of values.
     */
    Collection<V> getValues();

    /**
     * @return collection of keys.
     */
    Collection<K> getKeys();

}

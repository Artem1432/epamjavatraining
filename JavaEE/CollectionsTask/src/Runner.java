import implementation.LFUCache;
import implementation.LRUCache;
import interfaces.Cache;

/**
 * Class with main function.
 */
public class Runner {

    /**
     * Method main.
     * @param args - console parameters.
     */
    public static void main(final String[] args) {
        final Cache<Integer, Integer> lruCache = new LRUCache<>(6);
        final Cache<Integer, Integer> lfuCache = new LFUCache<>(6, 0.25);
        final Thread thread1 = new Thread(() -> {
            lruCache.putValue(1, 10);
            lruCache.putValue(2, 11);
            lruCache.putValue(3, 12);
        });
        thread1.start();
        final Thread thread2 = new Thread(() -> {
            lruCache.putValue(4, 13);
            lruCache.putValue(5, 14);
            lruCache.putValue(6, 15);
        });
        thread2.start();
        final Thread thread3 = new Thread(() -> {
            lfuCache.putValue(1, 10);
            lfuCache.putValue(2, 11);
            lfuCache.putValue(3, 12);
        });
        thread3.start();
        final Thread thread4 = new Thread(() -> {
            lfuCache.putValue(4, 13);
            lfuCache.putValue(5, 14);
            lfuCache.putValue(6, 15);
        });
        thread4.start();
        try {
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
            System.out.println("LRU cache:");
            lruCache.getValues().stream()
                    .forEach(System.out::println);
            System.out.println("LFU cache:");
            lfuCache.getValues().stream()
                    .forEach(System.out::println);
        } catch (InterruptedException e) {
            System.out.println("Thread error!");
        }
    }
}

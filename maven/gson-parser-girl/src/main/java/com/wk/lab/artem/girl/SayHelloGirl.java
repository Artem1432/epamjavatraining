package com.wk.lab.artem.girl;

import com.google.gson.JsonSyntaxException;
import com.wk.lab.artem.common.JsonFileReader;
import com.wk.lab.artem.common.Human;
import com.wk.lab.artem.common.SayHello;

import java.io.File;
import java.io.FileNotFoundException;

public class SayHelloGirl implements SayHello {

    public static void main(String[] args) {
        try {
            SayHello sayHello = new SayHelloGirl();
            sayHello.sayHello((Human) new JsonFileReader().
                    getObjectList(new File(args[0]), Human.class));
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (JsonSyntaxException e) {
            System.out.println("Неверный JSON объект");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Отсутствует параметр");
        }
    }

    public void sayHello(Human human) {
        System.out.println("Hello, girl! " + human);
    }
}

package com.wk.lab.artem.common;

import com.google.gson.Gson;

import java.io.FileReader;

public class GsonDataParser {

    private static Gson gson;

    public GsonDataParser() {
        if (gson == null) {
            gson = new Gson();
        }
    }

    public Object getObjectFromJson(FileReader json, Class clazz) {
        return gson.fromJson(json, clazz);
    }

}

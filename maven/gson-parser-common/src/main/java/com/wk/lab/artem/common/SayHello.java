package com.wk.lab.artem.common;

@FunctionalInterface
public interface SayHello {

    void sayHello(Human human);

}

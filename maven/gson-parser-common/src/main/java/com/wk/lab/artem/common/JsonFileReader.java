package com.wk.lab.artem.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class JsonFileReader {

    private GsonDataParser parser = new GsonDataParser();

    public Object getObjectList(File file, Class clazz) throws FileNotFoundException {
        return parser.getObjectFromJson(new FileReader(file), clazz);
    }

}

package exception;

/**
 * User exception for dao.
 */
public class DaoException extends RuntimeException {

    /**
     * Empty constructor.
     */
    public DaoException() {
    }

    /**
     * Constructor for error message.
     * @param message - error message.
     */
    public DaoException(final String message) {
        super(message);
    }

    /**
     * Constructor for error.
     * @param cause - real error.
     */
    public DaoException(final Exception cause) {
        super(cause);
    }

    /**
     * Constructor for error and message.
     * @param message - error message.
     * @param cause - real error.
     */
    public DaoException(final String message, final Exception cause) {
        super(message, cause);
    }

}

package listener;

import javax.servlet.AsyncEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Logger for AsyncContext.
 */
public class AsyncListener implements javax.servlet.AsyncListener {
    private static final Logger LOGGER = Logger.getLogger(AsyncListener.class.getName());

    @Override
    public void onComplete(final AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.SEVERE, "Async thread is finished.");
    }

    @Override
    public void onTimeout(final AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.SEVERE, "Async thread has timeout.");
    }

    @Override
    public void onError(final AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.SEVERE, "Async thread has error.");
    }

    @Override
    public void onStartAsync(final AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.SEVERE, "Async thread has new cycle.");
    }
}

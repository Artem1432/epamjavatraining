package listener;

import constant.Constants;
import implementation.IBooksImplDB;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constant.Constants.*;

/**
 * Context listener.
 */
@WebListener
public class ContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ContextListener.class.getName());

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        LOGGER.log(Level.SEVERE, servletContext.getInitParameter(IMPORTANT_INFO_INIT_PARAMETER));
        servletContext.setAttribute(BOOKS_DAO_ATTRIBUTES_NAME, new IBooksImplDB());
        realPath = servletContext.getRealPath("");
        servletContext.setAttribute(COVER_MIME_TYPE_ATTR_NAME, servletContext.
                getInitParameter(Constants.COVER_MIME_TYPE_INIT_PARAM));
        servletContext.setAttribute(COVER_FOLDER_ATTR, Constants.realPath + Constants.COVER_FOLDER);
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {

    }
}

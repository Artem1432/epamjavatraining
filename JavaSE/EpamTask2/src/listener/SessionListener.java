package listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebListener

public class SessionListener implements HttpSessionListener {
    private static final Logger LOGGER = Logger.getLogger(SessionListener.class.getName());
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("hh.mm.ss, dd.MM.yyyy");

    @Override
    public void sessionCreated(final HttpSessionEvent httpSessionEvent) {
        LOGGER.log(Level.SEVERE, "Session " + httpSessionEvent.getSession().getId()
                + " is created at " + DATE_FORMAT.format(new Date()));
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent httpSessionEvent) {
        LOGGER.log(Level.SEVERE, "Session " + httpSessionEvent.getSession().getId()
                + " is destroyed at " + DATE_FORMAT.format(new Date()));
    }
}

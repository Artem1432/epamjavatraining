package tagClasses;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.File;
import java.io.IOException;

/**
 * Simple tag.
 */
public class SimpleTag extends SimpleTagSupport {
    private String url;

    @Override
    public void doTag() throws JspException, IOException {
        final PageContext pageContext = (PageContext) getJspContext();
        pageContext.setAttribute("url", url);
        final String resource = pageContext.getServletContext().getRealPath(url);
        if (new File(resource).exists()) {
            pageContext.setAttribute("resource", resource);
        } else {
            pageContext.setAttribute("resource", "none");
        }
        getJspBody().invoke(null);
    }

    /**
     * Url setter.
     * @param url - url attr value.
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return url.
     */
    public String getUrl() {
        return url;
    }
}

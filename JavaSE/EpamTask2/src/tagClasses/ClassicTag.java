package tagClasses;

import bean.UrlMapping;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;

/**
 * Classic tag.
 */
public class ClassicTag extends TagSupport {
    private final Deque<UrlMapping> mappingDeque = new LinkedList<>();

    @Override
    public int doStartTag() throws JspException {
        final Map<String, ? extends ServletRegistration> registrations =
                pageContext.getServletContext().getServletRegistrations();
        for (Map.Entry<String, ? extends ServletRegistration> entry : registrations.entrySet()) {
            mappingDeque.addFirst(new UrlMapping(entry.getValue().
                    getMappings().toString(), entry.getKey()));
        }
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {
        if (!mappingDeque.isEmpty()) {
            pageContext.setAttribute("url",
                    mappingDeque.getLast().getUrl());
            pageContext.setAttribute("servlet",
                    mappingDeque.getLast().getResourceName() + "<br>");
            mappingDeque.removeLast();
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }
}

package myWebFragment;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

//@WebServlet(value = "/imageUploader", loadOnStartup = 0)
//@MultipartConfig
public class ImageUploadController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ImageUploadController.class.getName());

    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("imageUploader", "/imageUploader");
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        int bookId = Integer.parseInt(req.getParameter("id"));
        final Part imgPart = req.getPart("file" + bookId);
        try {
            if (!upload((String) getServletContext().getAttribute("coverMimeType"),
                    bookId, imgPart, (String) getServletContext().getAttribute("coverFolder"))) {
                req.getSession().setAttribute("error", "Bad file format.");
                LOGGER.log(Level.SEVERE, "Unsuccessful upload.");
            } else {
                LOGGER.log(Level.SEVERE, "Successful upload.");
            }
        } catch (ClassCastException e) {
            LOGGER.log(Level.SEVERE, "Error of upload.");
            req.getSession().setAttribute("error", "File is not chosen.");
        }
        resp.sendRedirect("/bookGetter");
    }

    @Override
    public void destroy() {
        getServletContext().removeAttribute("imageUploader");
    }

    private boolean upload(final String coverMimeType, final int bookId, final Part imgPart, final String destFolder)
            throws IOException {
        if (!imgPart.getContentType().equals(coverMimeType)) {
            return false;
        }
        final String fileExtension = getFileExtension(
                Paths.get(imgPart.getSubmittedFileName()).getFileName().toString());
        final String fileName = bookId + fileExtension;
        new File(destFolder).mkdir();
        imgPart.write(destFolder + "/" + fileName);
        return true;
    }

    private String getFileExtension(final String fileName) {
        int index = fileName.lastIndexOf(".");
        return index == -1 ? "" : fileName.substring(index);
    }
}

package implementation;

import bean.Book;
import database.DBConnection;
import exception.DaoException;
import interfaces.IBooksDAO;

import java.io.File;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constant.Constants.*;

/**
 * DAO for working with books from database.
 */
public class IBooksImplDB implements IBooksDAO {

    private static final Logger LOGGER = Logger.getLogger(IBooksImplDB.class.getName());

    private static final String GET_BOOKS_NAMES = "Select * from books";

    private static final String ADD_BOOK_REQUEST = "insert into books(author,booksName,description) "
                                                   + "values(?,?,?)";

    private static final String DELETE_BOOK_REQUEST = "delete from books where id = ";

    private static final String UPDATE_BOOK_REQUEST = "update books set author = ?,booksName = ?,description = ? "
                                                      + "where id = ";

    @Override
    public List<Book> getBooks() {
        try (Connection connection = DBConnection.connect()) {
            return askBooks(connection);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_CONNECTION_ERROR, e);
        }
    }

    @Override
    public boolean addBook(final Book book) {
        try (Connection connection = DBConnection.connect()) {
            boolean result = false;
            if (!askBooks(connection).contains(book)) {
                result = add(connection, book);
            }
            return result;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_CONNECTION_ERROR, e);
        }
    }

    @Override
    public boolean updateBook(final Book book) {
        try (Connection connection = DBConnection.connect()) {
            boolean result = false;
            if (!askBooks(connection).contains(book)) {
                result = update(connection, book);
            }
            return result;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_CONNECTION_ERROR, e);
        }
    }

    @Override
    public boolean deleteBook(final int bookId) {
        try (Connection connection = DBConnection.connect()) {
            return delete(connection, bookId);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_CONNECTION_ERROR, e);
        }
    }

    private List<Book> askBooks(final Connection connection) {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_BOOKS_NAMES);
            List<Book> bookList = new LinkedList<>();
            final int idPos = 1, authorPos = 2, bookNamePos = 3, descriptionPos = 4;
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getInt(idPos), resultSet.getString(authorPos),
                        resultSet.getString(bookNamePos), resultSet.getString(descriptionPos),
                        getCoverPath(resultSet.getInt(idPos))));
            }
            return bookList;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(ERROR_OF_ASKING_OF_BOOKS, e);
        } finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeResultSet(resultSet);
        }
    }

    private boolean add(final Connection connection, final Book book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(ADD_BOOK_REQUEST)) {
            populateStatementWithBookData(preparedStatement, book);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(ADDING_ERROR, e);
        }
    }

    private boolean update(final Connection connection, final Book book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BOOK_REQUEST + book.getId())) {
            populateStatementWithBookData(preparedStatement, book);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(UPDATING_ERROR, e);
        }
    }

    private boolean delete(final Connection connection, final int bookId) {
        try (Statement statement = connection.createStatement()) {
            statement.execute(DELETE_BOOK_REQUEST + bookId);
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DELETING_ERROR, e);
        }
    }

    private void populateStatementWithBookData(final PreparedStatement preparedStatement,
                                               final Book book) throws SQLException {
        final int authorPos = 1, bookNamePos = 2, descriptionPos = 3;
        preparedStatement.setString(authorPos, book.getAuthor());
        preparedStatement.setString(bookNamePos, book.getName());
        preparedStatement.setString(descriptionPos, book.getDescription());
    }

    private String getCoverPath(final int bookId) {
        final String jpgName = COVER_FOLDER + bookId + ".jpg";
        final String jpegName = COVER_FOLDER + bookId + ".jpeg";
        String result = null;
        if (new File(realPath + jpgName).exists()) {
            result = jpgName;
        } else if (new File(realPath + jpegName).exists()) {
            result = jpegName;
        }
        return result;
    }
}

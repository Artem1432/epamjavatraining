package constant;

/**
 * File of constants.
 */
public class Constants {

    /**
     * Error's texts.
     */
    public static final String DB_POOL_ERROR = "Error of connections pool!";
    public static final String DB_CONNECTION_ERROR = "Error of connection with database!";
    public static final String CLOSING_CONNECTION_ERROR = "Error of closing of connection!";
    public static final String CLOSING_STATEMENT_ERROR = "Error of closing of statement!";
    public static final String CLOSING_RESULT_SET_ERROR = "Error of closing of a result set!";
    public static final String ERROR_OF_ASKING_OF_BOOKS = "Error of asking of books!";
    public static final String ADDING_ERROR = "Error of adding!";
    public static final String UPDATING_ERROR = "Error of updating!";
    public static final String DELETING_ERROR = "Error of deleting!";
    public static final String BOOK_ALREADY_EXIST = "This book already exist!";
    public static final String BAD_BOOK_DATA = "Something field has bad data!";
    public static final String BOOK_NOT_CHOSEN = "Book is't chosen!";


    /**
     * Path to jsp or servlets.
     */
    public static final String BOOKS_JSP = "/books.jsp";
    public static final String BOOK_GETTER = "/bookGetter";

    /**
     * Attribute names for jsp.
     */
    public static final String BOOKS_DAO_ATTRIBUTES_NAME = "booksDAO";
    public static final String ERROR_ATTRIBUTES_NAME = "error";
    public static final String BOOK_LIST_ATTRIBUTES_NAME = "bookList";
    public static final String IMPORTANT_INFO_INIT_PARAMETER = "Important information";

    /**
     * Book parameter's names.
     */
    public static final String BOOK_ID = "id";
    public static final String BOOK_AUTHOR = "author";
    public static final String BOOK_NAME = "name";
    public static final String BOOK_DESCRIPTION = "description";

    /**
     * Empty string.
     */
    public static final String EMPTY_STRING = "";

    public static final String COVER_FOLDER_ATTR = "coverFolder";
    public static final String COVER_MIME_TYPE_ATTR_NAME = "coverMimeType";
    public static final String COVER_MIME_TYPE_INIT_PARAM = "CoverMimeType";
    public static final String COVER_FOLDER = "covers/";
    public static String realPath;
}

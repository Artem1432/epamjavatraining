import constant.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Test servlet for creating of short living sessions.
 */
@WebServlet("/sessionCreator")
@MultipartConfig
public class SessionCreateController extends AbstractServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        req.getSession().setMaxInactiveInterval(1);
        forward(Constants.BOOK_GETTER, req, resp);
    }
}

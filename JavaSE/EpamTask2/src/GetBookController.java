import exception.DaoException;
import interfaces.IBooksDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static constant.Constants.*;

/**
 * Servlet name.
 */
@WebServlet("/bookGetter")
@MultipartConfig

public class GetBookController extends AbstractServlet {

    private static IBooksDAO booksDAO;

    @Override
    public void init() throws ServletException {
        booksDAO = (IBooksDAO) getServletContext().getAttribute(BOOKS_DAO_ATTRIBUTES_NAME);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            req.setAttribute(BOOK_LIST_ATTRIBUTES_NAME, booksDAO.getBooks());
            forward(BOOKS_JSP, req, resp);
        } catch (DaoException e) {
            forwardError(BOOKS_JSP, e.getMessage(), req, resp);
        }
    }
}

package bean;

import constant.Constants;

/**
 * Bool implementation for database.
 */
public class Book {

    private Integer id;
    private String author;
    private String name;
    private String description;
    private String coverPath;

    /**
     * Create copy of book.
     * @param id in database.
     * @param author - field in database.
     * @param name - field in database.
     * @param description - field in database.
     */
    public Book(final int id, final String author, final String name, final String description) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.description = description;
    }

    /**
     * Create copy of book.
     * @param id in database.
     * @param author - field in database.
     * @param name - field in database.
     * @param description - field in database.
     * @param coverPath - path to cover image.
     */
    public Book(final int id, final String author, final String name, final String description, final String coverPath) {
        this(id, author, name, description);
        this.coverPath = coverPath;
    }

    /**
     * Set book id.
     * @param id in database.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Set book author.
     * @param author field in database.
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Set book name.
     * @param name field in database.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Set book description.
     * @param description field in database.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Set path to cover.
     * @param coverPath - path to cover.
     */
    public void setCoverPath(final String coverPath) {
        this.coverPath = coverPath;
    }

    /**
     * @return book id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return book author.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @return book name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return book description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return path to cover.
     */
    public String getCoverPath() {
        return coverPath;
    }

    /**
     * @return true if something field is empty.
     */
    public boolean emptyFieldTest() {
        final String emptyString = Constants.EMPTY_STRING;
        return emptyString.equals(author) || emptyString.equals(name)
                || emptyString.equals(description);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        boolean result = false;
        if (obj instanceof Book) {
            Book book = (Book) obj;
            result = author.equals(book.author) && name.equals(book.name);
        }
        return result;
    }

    @Override
    public int hashCode() {
        return author.hashCode() + name.hashCode();
    }
}

package bean;

/**
 * Bean for tag task.
 */
public class UrlMapping {

    private String url;
    private String resourceName;

    public UrlMapping(final String url, final String resourceName) {
        this.url = url;
        this.resourceName = resourceName;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setResourceName(final String resourceName) {
        this.resourceName = resourceName;
    }

    public String getUrl() {
        return url;
    }

    public String getResourceName() {
        return resourceName;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        final UrlMapping mapping = (UrlMapping) obj;
        return url.equals(mapping.url)
                && resourceName.equals(mapping.resourceName);
    }

    @Override
    public int hashCode() {
        return url.hashCode() + resourceName.hashCode();
    }
}

import bean.Book;
import exception.DaoException;
import interfaces.IBooksDAO;
import wrapper.BookServletRequestWrapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static constant.Constants.*;

/**
 * Servlet name.
 */
@WebServlet("/bookAdder")
@MultipartConfig

public class AddBooksController extends AbstractServlet {

    private static IBooksDAO booksDAO;

    @Override
    public void init() throws ServletException {
        booksDAO = (IBooksDAO) getServletContext().getAttribute(BOOKS_DAO_ATTRIBUTES_NAME);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        req.getSession().removeAttribute(ERROR_ATTRIBUTES_NAME);
        try {
            Book book = new Book(0, req.getParameter(BOOK_AUTHOR),
                    req.getParameter(BOOK_NAME), req.getParameter(BOOK_DESCRIPTION));
            if (!book.emptyFieldTest()) {
                if (!booksDAO.addBook(book)) {
                    setError(BOOK_ALREADY_EXIST, req);
                }
            } else {
                setError(BAD_BOOK_DATA, req);
            }
        } catch (DaoException e) {
            setError(e.getMessage(), req);
        }
        resp.sendRedirect(BOOK_GETTER);
    }
}

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Async servlet.
 */
@WebServlet(value = "/asyncServlet")
@MultipartConfig
public class AsyncController extends AbstractServlet {
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("asyncMsg", "Async thread: It is " + Thread.currentThread().getName()
                + " at " + System.currentTimeMillis());
        forward("/async.jsp", req, resp);
    }
}

package database;

import exception.DaoException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constant.Constants.*;

/**
 * Permit to work with database.
 */
public class DBConnection {

    private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());

    /**
     * @return copy od connection from pool.
     * @throws SQLException when something problems appear.
     */
    public static Connection connect() throws SQLException {
        try {
            Context context = (Context) (new InitialContext().lookup("java:comp/env"));
            DataSource ds = (DataSource) context.lookup("jdbc/task1");
            return ds.getConnection();
        } catch (NamingException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_POOL_ERROR, e);
        }
    }

    /**
     * Close opened connection.
     * @param connection - closing connection.
     */
    public static void closeConnection(final Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(CLOSING_CONNECTION_ERROR, e);
        }
    }

    /**
     * Close result sets.
     * @param resultSets array.
     */
    public static void closeResultSet(final ResultSet... resultSets) {
        for (ResultSet resultSet: resultSets) {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, e.toString(), e);
                    throw new DaoException(CLOSING_RESULT_SET_ERROR, e);
                }
            }
        }
    }

    /**
     * Close statements.
     * @param statements array.
     */
    public static void closeStatement(final Statement... statements) {
        for (Statement statement: statements) {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, e.toString(), e);
                    throw new DaoException(CLOSING_STATEMENT_ERROR, e);
                }
            }
        }
    }



}

package wrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import java.util.regex.Pattern;

import static constant.Constants.*;

/**
 * Book wrapper for HttpServletRequest.
 */
public class BookServletRequestWrapper extends HttpServletRequestWrapper {
    private static final Pattern DATA_PATTERN = Pattern.compile("<script>[\\w\\W]*</script>");

    /**
     * Default constructor.
     * @param httpServletRequest exemplar.
     */
    public BookServletRequestWrapper(final HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    public String getParameter(final String name) {
        return validateData(super.getParameter(name));
    }

    private String validateData(final String dataString) {
        String result = dataString;
        if (dataString == null || DATA_PATTERN.matcher(dataString).matches()) {
            result = EMPTY_STRING;
        }
        return result;
    }

}

package interfaces;

import bean.Book;

import java.util.List;

/**
 * Interface for working with book source.
 */
public interface IBooksDAO {

    /**
     * @return list of books from data source.
     */
    List<Book> getBooks();

    /**
     * @param book for adding.
     * @return result of adding.
     */
    boolean addBook(Book book);

    /**
     * @param book for updating.
     * @return result of updating.
     */
    boolean updateBook(Book book);

    /**
     * @param bookId - id of deleted book.
     * @return result of deleting.
     */
    boolean deleteBook(int bookId);

}

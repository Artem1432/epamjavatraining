import exception.DaoException;
import interfaces.IBooksDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static constant.Constants.*;

/**
 * Servlet name.
 */
@WebServlet("/bookDeleter")
@MultipartConfig

public class DeleteBooksController extends AbstractServlet {

    private static IBooksDAO booksDAO;

    @Override
    public void init() throws ServletException {
        booksDAO = (IBooksDAO) getServletContext().getAttribute(BOOKS_DAO_ATTRIBUTES_NAME);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            booksDAO.deleteBook(Integer.parseInt(req.getParameter(BOOK_ID)));
        } catch (DaoException e) {
            setError(e.getMessage(), req);
        }
        resp.sendRedirect(BOOK_GETTER);
    }
}

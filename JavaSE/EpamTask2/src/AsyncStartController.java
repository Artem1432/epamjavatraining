import constant.Constants;
import listener.AsyncListener;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Async activator.
 */
@WebServlet(value = "/asyncStarter", asyncSupported = true)
@MultipartConfig
public class AsyncStartController extends AbstractServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final AsyncContext asyncContext = req.startAsync();
        asyncContext.addListener(new AsyncListener());
        try {
            asyncContext.start(() -> {
                asyncContext.dispatch("/asyncServlet");
            });
            req.setAttribute("mainMsg", "Main thread: It is " + Thread.currentThread().getName()
                    + " at " + System.currentTimeMillis());
        } catch (Exception e) {
            forward(Constants.BOOK_GETTER, req, resp);
        }
    }
}

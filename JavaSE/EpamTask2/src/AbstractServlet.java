import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static constant.Constants.ERROR_ATTRIBUTES_NAME;

/**
 * Servlet template.
 */
public abstract class AbstractServlet extends HttpServlet {

    protected void forward(final String path, final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(path);
        rd.forward(req, resp);
    }

    protected void forwardError(final String path, final String errorMessage, final HttpServletRequest req,
                                final HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute(ERROR_ATTRIBUTES_NAME, errorMessage);
        RequestDispatcher rd = getServletContext().getRequestDispatcher(path);
        rd.forward(req, resp);
    }

    protected void setError(final String errorMessage, final HttpServletRequest req) {
        req.getSession().setAttribute(ERROR_ATTRIBUTES_NAME, errorMessage);
    }

}

<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 15.02.2019
  Time: 1:14
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="customTag" prefix="ctg" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tags</title>
</head>
<body>
    <ctg:urlList>
        ${url} - ${servlet}
    </ctg:urlList>
    <br>
    <ctg:urlResource url="/books.jsp">
        ${url} - ${resource}
    </ctg:urlResource>
</body>
</html>

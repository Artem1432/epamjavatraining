function setBookId(idNumber) {
    var kod = document.getElementById('kod');
    kod.value = document.getElementById('id' + idNumber).value;
}

function setActionAndSubmin(action) {
    document.booksForm.action = action;
    document.booksForm.submit();
}

function setUpdatingData(id) {
    document.getElementById('auth').value = document.getElementById(id + '1').value;
    document.getElementById('nm').value = document.getElementById(id + '2').value;
    document.getElementById('desc').value = document.getElementById(id + '3').value;
}

function setFile(bookId) {
    document.getElementById('fl').value = document.getElementById('file' + bookId).value;
}
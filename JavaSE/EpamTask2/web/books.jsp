<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 25.11.2018
  Time: 21:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of books</title>
    <script src="js/jsBookFunctions.js"></script>
</head>
<body>
    ${sessionScope.error}
    <form method="post" name="booksForm" enctype="multipart/form-data">
        <table border="1">
            <caption> Books </caption>
            <tr>
                <th>Cover</th>
                <th>Author</th>
                <th>Books name</th>
                <th>Description</th>
                <th colspan="2"></th>
            </tr>
            <c:forEach var="book" items="${requestScope.bookList}" varStatus="i">
                <tr valign="top">
                    <td>
                        <c:if test="${book.coverPath == null}">
                            <input type="file" name="file${book.id}" id="file${book.id}">
                            <c:if test="${applicationScope.imageUploader != null}">
                                <input type="hidden" id="uploader" value="${applicationScope.imageUploader}">
                                <input type="button" value="Upload" name="${book.id}"
                                       onclick="setBookId(this.name);
                                       document.getElementById('fl').value = document.getElementById('file' + this.name).value;
                                       setActionAndSubmin(document.getElementById('uploader').value)">
                            </c:if>
                            <c:if test="${applicationScope.imageUploader == null}">
                                <input type="button" value="Upload" disabled>
                            </c:if>
                        </c:if>
                        <c:if test="${book.coverPath != null}">
                            <img src="${book.coverPath}" height="300" width="300">
                        </c:if>
                    </td>
                    <td>
                        <input type="text" value="<c:out value="${book.author}"/>" readonly="true" id="${book.id}1">
                    </td>
                    <td>
                        <input type="text" value="<c:out value="${book.name}"/>" readonly="true" id="${book.id}2">
                    </td>
                    <td>
                        <input type="text" value="<c:out value="${book.description}"/>" readonly="true" id="${book.id}3">
                    </td>
                    <td>
                        <input type="button"
                               name="${book.id}"
                               value="Edit"
                               onclick="setBookId(this.name); setUpdatingData(this.name)">
                    </td>
                    <td>
                        <input type="hidden" id="id${book.id}" value="${book.id}">
                        <input type="button" value="Delete"
                               name="${book.id}"
                               onclick="setBookId(this.name); setActionAndSubmin('/bookDeleter')"
                               style="width: 100%">
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td>
                    <input type="hidden" name="file" id="fl">
                </td>
                <td>
                    <input type="text" name = "author" id = "auth">
                </td>
                <td>
                    <input type="text" name = "name" id = "nm">
                </td>
                <td>
                    <input type="text" name = "description" id = "desc">
                </td>
                <td>
                    <input type="button" value="Add" onclick="setActionAndSubmin('/bookAdder')">
                </td>
                <td>
                    <input type="hidden" name="id" id="kod">
                    <input type="button" value="Update" onclick="setActionAndSubmin('/bookUpdater')">
                </td>
            </tr>
        </table>
    </form>
    <a href="/sessionCreator">Create session</a>
    <a href="/asyncStarter">Go to async</a>
    <a href="/tags.jsp">Tag task</a>
</body>
</html>

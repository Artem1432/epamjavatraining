<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 25.11.2018
  Time: 21:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of books</title>
</head>
<body>
    ${requestScope.error}
    <c:forEach var="book" items="${requestScope.bookList}" varStatus="i">
        ${book.id}  ${book.name} <br>
    </c:forEach>
    <a href = "/bookGetter">Перезайти для получения/не получения недоступности</a>
</body>
</html>

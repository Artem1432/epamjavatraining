package listener;

import static constant.Constants.*;
import database.DBConnection;
import implementation.BooksImplDB;
import interfaces.BooksDAO;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebListener

public class ContListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ContListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOGGER.log(Level.SEVERE, servletContextEvent.getServletContext().getInitParameter(IMPORTANT_INFO_INIT_PARAMETER));
        servletContextEvent.getServletContext().setAttribute(UNAVAILABLE_FLAG_ATTRIBUTES_NAME, 0);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}

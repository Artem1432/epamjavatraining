package database;

import static constant.Constants.*;
import exception.DaoException;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());


    public static Connection connect() throws SQLException{
        try{
        Connection connection;
        Class.forName(DATABASE_DRIVER);
        connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
        return connection;
        }catch (ClassNotFoundException e){
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_DRIVER_ERROR, e);
        }
    }

    public static void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(CLOSING_CONNECTION_ERROR, e);
        }
    }

    public static void closeResultSet(ResultSet... resultSets) {
        for (ResultSet resultSet: resultSets) {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, e.toString(), e);
                    throw new DaoException(CLOSING_RESULT_SET_ERROR, e);
                }
            }
        }
    }

    public static void closeStatement(Statement... statements) {
        for (Statement statement: statements) {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, e.toString(), e);
                    throw new DaoException(CLOSING_STATEMENT_ERROR, e);
                }
            }
        }
    }



}

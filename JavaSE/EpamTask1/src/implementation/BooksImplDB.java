package implementation;

import bean.Book;
import static constant.Constants.*;
import database.DBConnection;
import exception.DaoException;
import interfaces.BooksDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BooksImplDB implements BooksDAO {

    private static final Logger LOGGER = Logger.getLogger(BooksImplDB.class.getName());

    private static final String GET_BOOKS_NAMES = "Select * from books";

    @Override
    public List<Book> getBooksNames() {
        try(Connection connection = DBConnection.connect()){
            return askBooksNames(connection);
        }catch ( SQLException e){
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(DB_CONNECTION_ERROR, e);
        }
    }

    private List<Book> askBooksNames(Connection connection) {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_BOOKS_NAMES);
            List<Book> bookList = new LinkedList<>();
            while(resultSet.next()){
                bookList.add(new Book(resultSet.getInt(1), resultSet.getString(2)));
            }
            return bookList;
        }catch (SQLException e){
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(ERROR_OF_ASKING_OF_BOOKS,e);
        } finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeResultSet(resultSet);
        }
    }
}

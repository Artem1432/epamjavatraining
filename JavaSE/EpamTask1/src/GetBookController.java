import static constant.Constants.*;
import exception.DaoException;
import implementation.BooksImplDB;
import interfaces.BooksDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/bookGetter")

public class GetBookController extends AbstractServlet {

    private static BooksDAO booksDAO;

    @Override
    public void init() throws ServletException {
        booksDAO = new BooksImplDB();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        throwUnavailableException();
        try {
            req.setAttribute(BOOK_LIST_ATTRIBUTES_NAME, booksDAO.getBooksNames());
            forward(req,resp);
        } catch (DaoException e){
            forwardError(e.getMessage(),req,resp);
        }
    }

    private void throwUnavailableException() throws UnavailableException {
        ServletContext servletContext = getServletContext();
        int unAvFlag = (int) servletContext.getAttribute(UNAVAILABLE_FLAG_ATTRIBUTES_NAME);
        if(unAvFlag == 0)
            servletContext.setAttribute(UNAVAILABLE_FLAG_ATTRIBUTES_NAME, 1);
        else {
            servletContext.setAttribute(UNAVAILABLE_FLAG_ATTRIBUTES_NAME, 0);
            throw new UnavailableException("At this time servlet is unavailable.", TIME_OF_UNAVAILABILITY);
        }
    }
}

package constant;

public class Constants {

    public static final String DAO_TYPE = "sql";

    public static final String DATABASE_DRIVER = "org.gjt.mm.mysql.Driver";
    public static final String DATABASE_URL = "jdbc:mysql://localhost/task1";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "400000608";

    public static final String DB_DRIVER_ERROR = "MySql driver is not found!";
    public static final String DB_CONNECTION_ERROR = "Error of connection with database!";
    public static final String CLOSING_CONNECTION_ERROR = "Error of closing of connection!";
    public static final String CLOSING_STATEMENT_ERROR = "Error of closing of statement!";
    public static final String CLOSING_RESULT_SET_ERROR = "Error of closing of a result set!";
    public static final String ERROR_OF_ASKING_OF_BOOKS = "Error of asking of books!";

    public static final int TIME_OF_UNAVAILABILITY = 3;

    public static final String BOOKS_JSP = "/books.jsp";
    public static final String BOOKS_DAO_ATTRIBUTES_NAME = "booksDAO";
    public static final String UNAVAILABLE_FLAG_ATTRIBUTES_NAME = "unAvFlag";
    public static final String ERROR_ATTRIBUTES_NAME = "error";
    public static final String BOOK_LIST_ATTRIBUTES_NAME = "bookList";
    public static final String IMPORTANT_INFO_INIT_PARAMETER = "Important information";
}

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static constant.Constants.BOOKS_JSP;
import static constant.Constants.ERROR_ATTRIBUTES_NAME;

public abstract class AbstractServlet extends HttpServlet {

    protected void forward(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(BOOKS_JSP);
        rd.forward(req, resp);
    }

    protected void forwardError(String errorMessage, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute(ERROR_ATTRIBUTES_NAME, errorMessage);
        RequestDispatcher rd = getServletContext().getRequestDispatcher(BOOKS_JSP);
        rd.forward(req, resp);
    }

}

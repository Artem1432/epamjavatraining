package interfaces;

import bean.Book;

import java.util.List;

public interface BooksDAO {

    List<Book> getBooksNames();

}
